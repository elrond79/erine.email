#!/bin/bash

if [ -f /var/backups/.mariadb.pwd ]
then
  echo "This script should be called once, by Puppet only"
  exit 1
fi

set -e

# Create the backup user
PASSWORD=$(/bin/mktemp -u XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX)
echo "${PASSWORD}" > /var/backups/.mariadb.pwd
chown backup:backup /var/backups/.mariadb.pwd
chmod 400 /var/backups/.mariadb.pwd
/usr/bin/mysql -p"$(/bin/cat /root/.mariadb.pwd)" -e "CREATE USER backup@localhost IDENTIFIED BY \"${PASSWORD}\";"

# Grant backup user
# We do not need the LOCK TABLE permissions, as we use mysqldump with the
# --single-transaction option
/usr/bin/mysql -p"$(/bin/cat /root/.mariadb.pwd)" -e 'GRANT SELECT ON spameater.* TO backup@localhost;'
