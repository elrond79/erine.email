#!/usr/bin/python3

import argparse
import datetime
import dropbox
import json
import logging
import os
import re
import requests
import subprocess
import sys


CONFIG_FILE = "/var/backups/.config/daily-bkp/auth.json"


class dump:
    def __init__(self, filename):
        self.filename = filename
        r = re.match("spameater\.(.+)\.sql(|\.encrypted)$", filename)
        if not r:
            self.valid = False
        else:
            try:
                self.datetime = datetime.datetime.strptime(r.groups()[0], "%Y-%m-%d_%H%M%S_%z")
            except:
                self.valid = False
            else:
                self.valid = True
                self.encrypted = r.groups()[1] == ".encrypted"


def runCommand(command, logOnFailure):
    """Execute command

    Exit 1 on failure
    Only show stdout and stderr on failure
    """
    try:
        completed = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except Exception as e:
        logging.error(e)
        sys.exit(1)
    else:
        if completed.returncode != 0:
            logging.error('"{0}" completed with a {1} return code'.format(command, completed.returncode))
            if completed.stdout:
                logging.info("stdout:\n{0}".format(completed.stdout.decode("utf-8")))
            if completed.stderr:
                logging.warn("stderr:\n{0}".format(completed.stderr.decode("utf-8")))
            logging.error(logOnFailure)
            sys.exit(1)


def upload(dbx, filename):
    """Upload a file on Dropbox

    Raise on error
    """
    with open(filename, "rb") as f:
        file_content = f.read()
    result = dbx.files_upload(
        file_content, "/{0}".format(os.path.basename(filename)), dropbox.files.WriteMode.overwrite, mute=True
    )
    logging.info("{0} uploaded on Dropbox".format(filename))


def to_clean(filelist):
    """Browse filelist and return the dumps to remove"""

    # Generate old_dumps, a dictionary of the dumps older than 31 days, with
    # year-month keys. Example:
    # {
    #   '2021-05': [<dump object 1>, <dump object 2>],
    #   '2021-06': [<dump object 3>, <dump object 4>]
    # }
    old_dumps = {}
    for dump_file in filelist:
        if dump_file.datetime > datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=31):
            continue
        year_month = dump_file.datetime.strftime("%Y-%m")
        month_dumps = old_dumps.get(year_month, [])
        month_dumps.append(dump_file)
        old_dumps[year_month] = month_dumps

    # Keep the first dump of the month, remove the others
    dumps_to_clean = []
    for year_month in old_dumps:
        month_dumps = sorted(old_dumps[year_month], key=lambda x: x.datetime)
        kept_dump = month_dumps.pop(0)
        for dump_file in month_dumps:
            dumps_to_clean.append(dump_file)
    return dumps_to_clean


def clean_dbx_folder(dbx):
    """Clean the Dropbox application root folder, containing the dumps

    Abort the cleanup on error
    """
    try:
        result = dbx.files_list_folder("")
    except dropbox.exceptions.ApiError as e:
        logging.error("Folder listing failed: {0} ; Cleanup aborted".format(e))
        return
    if result.has_more:
        logging.warning('Unexpected "has_more" found ; The cleanup should be incomplete')
    dump_files = []
    unexpected_folders = 0
    unexpected_files = 0
    for entry in result.entries:
        if isinstance(entry, dropbox.files.FolderMetadata):
            unexpected_folders += 1
            continue
        dump_file = dump(entry.name)
        if dump_file.valid:
            dump_files.append(dump_file)
        else:
            unexpected_files += 1
    if unexpected_folders:
        logging.warning("{0} unexpected folders found on Dropbox ; They had been skipped".format(unexpected_folders))
    if unexpected_files:
        logging.warning("{0} unexpected files found on Dropbox ; They had been skipped".format(unexpected_files))
    for dump_file in to_clean(dump_files):
        try:
            dbx.files_delete(path="/{0}".format(dump_file.filename))
            logging.info("{0} removed from Dropbox".format(dump_file.filename))
        except dropbox.exceptions.ApiError as e:
            logging.error("Can not remove {0}: {1} ; Cleanup aborted".format(dump_file.filename, e))
            return


def get_dbx_refresh_token(app_key, app_secret):
    """Get interactively a Dropbox refresh_token"""
    print()
    print("Go to the following URL, and validate the authorization:")
    url = "https://www.dropbox.com/oauth2/authorize"
    print("{0}?client_id={1}&response_type=code&token_access_type=offline".format(url, app_key))
    print()
    print("Dropbox gave you a temporary code. I need it to generate a token.")
    code = input("Code: ")
    payload = {"code": code, "grant_type": "authorization_code", "client_id": app_key, "client_secret": app_secret}
    r = requests.post(
        url="https://api.dropboxapi.com/oauth2/token",
        data=payload,
    )
    if r.status_code != 200:
        print("ERROR - The Dropbox API returned {0}:".format(r.status_code))
        print(r.text)
        sys.exit(1)
    answer = json.loads(r.text)
    if "refresh_token" not in answer:
        print("ERROR - refresh_token not found on Dropbox API answer")
        print(r.text)
        sys.exit(1)
    test_dbx_refresh_token(answer["refresh_token"], app_key, app_secret)
    return answer["refresh_token"]


def test_dbx_refresh_token(refresh_token, app_key, app_secret):
    """Test the Dropbox refresh_token

    Exit 1 on failure
    """
    with dropbox.Dropbox(oauth2_refresh_token=refresh_token, app_key=app_key, app_secret=app_secret) as dbx:
        try:
            dbx.users_get_current_account()
        except dropbox.exceptions.AuthError:
            logging.error("Invalid Dropbox refresh_token")
            sys.exit(1)


def main():

    # Get options
    parser = argparse.ArgumentParser(description="Make a backup, encrypt and upload it")
    parser.add_argument("--interactive", action="store_true", help="run the script in interactive mode")
    args = parser.parse_args()

    # Be sure /var/log/daily-bkp/daily-bkp.log exists and is accessible to backup user
    logging.root.setLevel(logging.INFO)
    logformat = logging.Formatter("%(asctime)s %(levelname)8s %(message)s", datefmt="%Y-%m-%d %H:%M:%S %Z")
    console = logging.StreamHandler(sys.stdout)
    console.setFormatter(logformat)
    logging.root.addHandler(console)
    file_handler = logging.FileHandler("/var/log/daily-bkp/daily-bkp.log")
    file_handler.setFormatter(logformat)
    logging.root.addHandler(file_handler)

    # Retrieve password
    try:
        with open("/var/backups/.mariadb.pwd", "r") as fd:
            password = fd.readline().strip()
    except Exception as e:
        logging.error(e)
        sys.exit(1)

    # Retrieve Dropbox configuration
    try:
        with open(CONFIG_FILE, "r") as f:
            configuration = f.readlines()
        configuration = json.loads("".join(configuration))
    except Exception as e:
        logging.error("Can not read {0}".format(CONFIG_FILE))
        logging.error("Fix this reading https://gitlab.com/mdavranche/erine.email/-/wikis/On-premises/Backups")
        sys.exit(1)
    conf_error = False
    for item in ["app_key", "app_secret"]:
        if item not in configuration:
            logging.error("{0} not found in {1}".format(item, CONFIG_FILE))
            conf_error = True
        if conf_error:
            logging.error("Fix this reading https://gitlab.com/mdavranche/erine.email/-/wikis/On-premises/Backups")
            sys.exit(1)
        app_key = configuration["app_key"]
        app_secret = configuration["app_secret"]
    if "refresh_token" not in configuration:
        if not args.interactive:
            logging.error(
                "refresh_token not found in {0} ; Use the --interactive option to set it up".format(CONFIG_FILE)
            )
            sys.exit(1)
        logging.warning("refresh_token not found in {0}".format(CONFIG_FILE))
        refresh_token = get_dbx_refresh_token(app_key, app_secret)
        configuration["refresh_token"] = refresh_token
        with open(CONFIG_FILE, "w") as f:
            f.write(json.dumps(configuration))
        logging.info("refresh_token retrieved and saved")
    else:
        refresh_token = configuration["refresh_token"]
        test_dbx_refresh_token(refresh_token, app_key, app_secret)

    # Set now, the current date and time
    # We use timezone.utc not because we're interested about UTC, but to have an object aware of the timezone (we could have
    # used any other timezone)
    now = datetime.datetime.now(datetime.timezone.utc)

    # Adjust "now" in local timezone
    now = now.astimezone()

    # Set dumpFile
    dumpFile = "/var/backups/spameater/spameater.{0}.sql".format(now.strftime("%Y-%m-%d_%H%M%S_%z"))

    # Check dumpFile
    # As it contains a timestamp accurate to the second, matching this condition probably means you're running this script
    # several times at the same time.
    if os.path.isfile(dumpFile):
        logging.error("{0} already exists".format(dumpFile))
        logging.error("Dump aborted")
        sys.exit(1)

    # Dump spameater database in dumpFile
    # We use --single-transaction as we exclusively use InnoDB
    logging.info("Dumping spameater database to {0}...".format(dumpFile))
    command = [
        "/usr/bin/mysqldump",
        "-u",
        "backup",
        "-p{0}".format(password),
        "--result-file={0}".format(dumpFile),
        "--single-transaction",
        "spameater",
    ]
    runCommand(command, logOnFailure="Dump aborted")
    logging.info("Dump completed successfully")

    # Encrypt backup
    logging.info("Encrypting {0} to {0}.encrypted...".format(dumpFile))
    command = [
        "/usr/bin/gpg",
        "--output",
        "{0}.encrypted".format(dumpFile),
        "--default-recipient-self",
        "--encrypt",
        dumpFile,
    ]
    runCommand(command, logOnFailure="Encryption aborted")
    os.remove(dumpFile)
    logging.info("Dump encrypted successfully")
    dumpFile = "{0}.encrypted".format(dumpFile)

    # Push the dump on Dropbox
    with dropbox.Dropbox(oauth2_refresh_token=refresh_token, app_key=app_key, app_secret=app_secret) as dbx:
        upload(dbx, dumpFile)

    # Remove local dump
    os.remove(dumpFile)

    # Clean Dropbox folder
    clean_dbx_folder(dbx)


if __name__ == "__main__":
    main()
