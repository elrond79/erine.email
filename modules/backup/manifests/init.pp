# Daily dump database and push it to Dropbox
class backup {

  # /var/backups/ is the home dir of the "backup" default Debian user
  file { '/var/backups/updatedb.sh':
    ensure => present,
    mode   => '0740',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/backup/updatedb.sh',
  }

  exec { 'updateDb':
    command => '/var/backups/updatedb.sh',
    creates => '/var/backups/.mariadb.pwd',
    require => [
      Exec['mkDb'],
      File['/var/backups/updatedb.sh'],
    ],
  }

  # "backup" user and group are defaults ones on Debian
  file { '/usr/bin/daily-bkp':
    ensure => present,
    mode   => '0744',
    owner  => 'backup',
    group  => 'backup',
    source => 'puppet:///modules/backup/daily-bkp.py',
  }

  # Used by daily-bkp to store backups locally
  file { '/var/backups/spameater':
    ensure => directory,
    mode   => '0755',
    owner  => 'backup',
    group  => 'backup',
  }

  # Used to store the Dropbox authentication token
  file { '/var/backups/.config':
    ensure => directory,
    mode   => '0755',
    owner  => 'backup',
    group  => 'backup',
  }
  file { '/var/backups/.config/daily-bkp':
    ensure  => directory,
    mode    => '0700',
    owner   => 'backup',
    group   => 'backup',
    require => File['/var/backups/.config'],
  }
  file { '/var/backups/.config/daily-bkp/auth.json':
    ensure  => present,
    mode    => '0600',
    owner   => 'backup',
    group   => 'backup',
    require => File['/var/backups/.config/daily-bkp'],
  }

  file { '/var/backups/.gnupg':
    ensure => directory,
    mode   => '0700',
    owner  => 'backup',
    group  => 'backup',
  }

  file { '/var/spool/cron/crontabs/backup':
    ensure => present,
    mode   => '0600',
    owner  => 'backup',
    group  => 'crontab',
    source => 'puppet:///modules/backup/backup.cron',
  }

  file { '/var/log/daily-bkp':
    ensure => directory,
    mode   => '0755',
    owner  => 'backup',
    group  => 'backup',
  }

  file { '/etc/logrotate.d/daily-bkp':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/backup/daily-bkp.logrotate',
  }

}
