# Install the erine_email Python 3.9 package
class python_pkg {

  file { '/usr/local/lib/python3.9/dist-packages/erine_email':
    ensure  => directory,
    mode    => '2755',
    owner   => 'root',
    group   => 'staff',
    recurse => true,
    force   => true,
    purge   => true,
    source  => 'puppet:///modules/python_pkg/erine_email',
  }

}
