import re
import sqlalchemy

from erine_email.common import firstshot_regex, BounceException
from erine_email.generic import Generic
from functools import lru_cache


class FirstShot(Generic):
    """An erine.email user initiates a contact to a foreign address"""

    @property
    @lru_cache(maxsize=None)
    def outbound_sender(self):

        # r necessarily exists: the email() function already checked that
        r = re.search(firstshot_regex, self.inbound_recipient)
        return_value = "{0}.{1}@{2}".format(r.group(1), r.group(2), r.group(5))
        self.create_disposable_address(disposable_address=return_value, username=r.group(2))
        return return_value

    @property
    @lru_cache(maxsize=None)
    def outbound_recipient(self):

        # r necessarily exists: the email() function already checked that
        r = re.search(firstshot_regex, self.inbound_recipient)

        return "{0}@{1}".format(r.group(3), r.group(4))

    def check_usage_security(self):

        # r necessarily exists: the email() function already checked that
        r = re.search(firstshot_regex, self.inbound_recipient)

        query = sqlalchemy.text(
            "SELECT `mailAddress`, `reserved`, `activated` FROM `user` WHERE `username` = :username"
        )
        results = self.db_connection.execute(query, username=r.group(2))
        row = results.one_or_none()
        if not row:
            raise BounceException("FirstShot - Unknown user name: {0}".format(r.group(2)))
        if self.inbound_sender != row._mapping["mailAddress"]:
            raise BounceException(
                "FirstShot - {0} is not allowed to send an email as {1}".format(
                    self.inbound_sender, self.outbound_sender
                )
            )
        if row._mapping["reserved"]:
            raise BounceException(
                "FirstShot - Incorrect user usage: {0} exists, but as a reserved user".format(r.group(2))
            )
        if not row._mapping["activated"]:
            raise BounceException("FirstShot - The {0} user had not been activated".format(r.group(2)))

    def rewrite_from_replyto(self, address):
        if address[1] == self.inbound_sender:
            return (address[0], self.outbound_sender)
        return address

    def rewrite_to_cc_bcc(self, address):
        if address[1] == self.inbound_recipient:
            return (address[0], self.outbound_recipient)
        return address
