import random
import re
import sqlalchemy
import string

from erine_email.generic import Generic
from functools import lru_cache


class ClassicReserved(Generic):
    """A foreign address (most of time, a robot) writes to a "classic" or "reserved" user"""

    def _get_reply_address(self, foreign_address):
        """Retrieve and return mailAddress from the replyAddress DB table

        Create it if necessary
        """
        query = sqlalchemy.text(
            "SELECT `mailAddress` "
            "FROM `replyAddress` "
            "WHERE `disposableMailAddress` = :disposable_mail_address AND `foreignAddress` = :foreign_address"
        )
        results = self.db_connection.execute(
            query, disposable_mail_address=self.inbound_recipient, foreign_address=foreign_address
        )
        row = results.one_or_none()
        if row:
            return row._mapping["mailAddress"]
        else:

            # The local is a random string
            reply_address = "".join(random.choice(string.ascii_lowercase + string.digits) for _ in range(15))

            # Add the destination address domain
            # r necessarily exists: Postfix already checked that
            r = re.match(".+(@[^@]+)$", self.inbound_recipient)
            reply_address += r.group(1)

            query = sqlalchemy.text(
                "INSERT INTO `replyAddress` (`mailAddress`, `disposableMailAddress`, `foreignAddress`) "
                "VALUES (:mail_address, :disposable_mail_address, :foreign_address)"
            )
            self.db_connection.execute(
                query,
                mail_address=reply_address,
                disposable_mail_address=self.inbound_recipient,
                foreign_address=foreign_address,
            )
            return reply_address

    def rewrite_real_address(self, header_value):
        """Rewrite the user's real address found in headers different than the ones handled in Generic init"""
        return header_value.replace(self.outbound_recipient, self.inbound_recipient)

    @property
    @lru_cache(maxsize=None)
    def outbound_sender(self):
        return self._get_reply_address(self.inbound_sender)

    def rewrite_from_replyto(self, address):
        if address[0]:
            return_label = "{0} - {1}".format(address[0], address[1])
        else:
            return_label = address[1]
        return_address = self._get_reply_address(address[1])
        return (return_label, return_address)

    def rewrite_to_cc_bcc(self, address):
        return address
