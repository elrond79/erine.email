import abc
import email.header
import email.utils
import sqlalchemy


class Generic:
    """Generic email class

    outbound_message:
      The whole outbound email
      Format: email.message.Message object
    inbound_sender / inbound_recipient:
      The inbound sender / recipient ; Provided by Postfix
      Format: string representing an email address, without label
    outbound_sender / outbound_recipient:
      The outbound sender / recipient ; Provided to Sendmail
      Format: string representing an email address, without label
    message_id / subject:
      The message-id and subject found in the headers
      Format: string or None
    db_connection:
      The Connection to the DBMS
      Format: sqlalchemy.engine.base.Connection class
    "Inbound" is about what comes from the outside to the erine.email server
    "Outbound" is about what is intended to go outside from the erine.email server
    """

    def _rewrite_header(self, header_value, func):
        """Rewrite header_value using func

        header_value is a string representing either an email address or a list of email addresses
        func is a function getting and returning a single email address (tuple format)
        """

        # String to array of tuples
        current = email.utils.getaddresses([header_value])

        # Rewrite every tuple
        rewritten = [func(item) for item in current]

        # Array of tuples to string
        return ",".join([email.utils.formataddr(item) for item in rewritten])

    def __init__(self, inbound_message, inbound_sender, inbound_recipient, db_connection):
        """
        Keep inbound_sender, inbound_recipient, and db_connection as attributes, untouched ; Forge outbound_message

        inbound_message:
          The whole inbound email, converted to an object as is by spameater.py
          Format: email.message.Message object
          Warning: as an email.message.Message object is a mutable object, and as we want to have the smallest memory
          print, inbound_message will be altered! For more readability, the outbound_message attribute will point to
          the rewritten message.
        """
        self.inbound_sender = inbound_sender
        self.inbound_recipient = inbound_recipient
        self.db_connection = db_connection

        # To be done at the very beginning, to prevent wasting resources rewriting the email or inserting data in the
        # database. Ideally, all the raises (resulting in bouncing or deferring the email) should be done there.
        self.check_usage_security()

        # Rewrite headers
        self.outbound_message = inbound_message
        headers = self.outbound_message.items()
        self.message_id = None
        self.outbound_message._headers = []
        self.subject = None
        for (header_name, header_value) in headers:
            lower_header_name = header_name.lower()
            if lower_header_name in ["from", "reply-to"]:
                header_value = self._rewrite_header(header_value, self.rewrite_from_replyto)
            elif lower_header_name in ["to", "cc", "bcc"]:
                header_value = self._rewrite_header(header_value, self.rewrite_to_cc_bcc)
            elif lower_header_name == "message-id":

                # If Message-ID is absent, Postfix should add one. But if it is forced to empty (may exist even if not
                # RFC compliant), it will not. We handle this second case here.
                if not header_value:
                    header_value = email.utils.make_msgid()

                self.message_id = header_value
            elif lower_header_name == "subject":
                self.subject = header_value
            elif type(header_value) != email.header.Header:
                header_value = self.rewrite_real_address(header_value)
            self.outbound_message[header_name] = header_value

    def create_disposable_address(self, disposable_address, username):
        """Create disposable address if not exists

        Upsert syntax differs between PostgreSQL (our next DBMS) / SQLite3 (our unittest DBMS) and MySQL / MariaDB (our
        current DBMS). That's why we use 2 queries instead of 1. TODO: change that when we'll no longer use MariaDB.
        """

        # The record necessarily exists: the check_usage_security() functions already checked that
        query = sqlalchemy.text("SELECT `ID` FROM `user` WHERE `username` = :username")
        results = self.db_connection.execute(query, username=username)
        row = results.one_or_none()
        user_id = row._mapping["ID"]

        # As the operation is not atomic, it can fail in the very unprobable case where another INSERT had been done
        # between the SELECT and the INSERT. In that case, an exception will be raised by sqlalchemy, as the constraint
        # fails. This exception (and all the exceptions of this package) must be trapped on a higher level (the code
        # that calls this package) to defer the email.
        query = sqlalchemy.text("SELECT 1 FROM `disposableMailAddress` WHERE `mailAddress` = :disposable_address")
        results = self.db_connection.execute(query, disposable_address=disposable_address)
        if results.one_or_none():
            return
        query = sqlalchemy.text(
            "INSERT INTO `disposableMailAddress` (`mailAddress`, `userID`) VALUES (:disposable_address, :user_id)"
        )
        self.db_connection.execute(query, disposable_address=disposable_address, user_id=user_id)

    def rewrite_real_address(self, header_value):
        """Rewrite the user's real address found in headers different than the ones handled in Generic init

        This method is used for Reply and FirstShot, and overrided by ClassicReserved
        """
        return header_value.replace(self.inbound_sender, self.outbound_sender)

    @abc.abstractmethod
    def outbound_sender(self):
        pass  # pragma: no cover

    @abc.abstractmethod
    def outbound_recipient(self):
        pass  # pragma: no cover

    @abc.abstractmethod
    def check_usage_security(self):
        pass  # pragma: no cover

    @abc.abstractmethod
    def rewrite_from_replyto(self, address):
        """Rewrite an address (tuple format) found in the From or Reply-To header"""
        pass  # pragma: no cover

    @abc.abstractmethod
    def rewrite_to_cc_bcc(self, address):
        """Rewrite an address (tuple format) found in the To, CC or Bcc header"""
        pass  # pragma: no cover
