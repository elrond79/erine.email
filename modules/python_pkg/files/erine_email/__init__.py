"""erine_email

Copyright (C) 2017-2023 Mikael Davranche

This file is part of erine.email project. https://erine.email

erine.email is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with erine.mail.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import sqlalchemy

from erine_email.classic import Classic
from erine_email.common import classic_regex, firstshot_regex, reply_reserved_regex, BounceException
from erine_email.firstshot import FirstShot
from erine_email.reply import Reply
from erine_email.reserved import Reserved


def email(message, sender, recipient, db_connection):
    """Return an instance of the correct email class, depending on the recipient format

    Raise a BounceException on incorrect recipient format
    """
    if re.match(classic_regex, recipient):
        return Classic(message, sender, recipient, db_connection)
    elif re.match(reply_reserved_regex, recipient):
        query = sqlalchemy.text("SELECT 1 FROM `replyAddress` WHERE `mailAddress` = :mail_address")
        results = db_connection.execute(query, mail_address=recipient)
        row = results.one_or_none()
        if row:
            return Reply(message, sender, recipient, db_connection)
        else:
            return Reserved(message, sender, recipient, db_connection)
    elif re.match(firstshot_regex, recipient):
        return FirstShot(message, sender, recipient, db_connection)
    else:
        raise BounceException("Unknown recipient format: {0}".format(recipient))
