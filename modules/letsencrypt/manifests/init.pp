# Install Let's Encrypt certificates using Certbot
class letsencrypt {

  package { 'snapd':
    ensure => present,
    notify => Exec['installCore'],
  }

  # We can't use the "unless => '/usr/bin/snap list core'" method here, as "snap
  # list <snap>" returns 1 only if <snap> does not exist AND at least 1 snap is
  # installed.
  exec { 'installCore':
    command     => '/usr/bin/snap install core',
    user        => root,
    refreshonly => true,
    require     => Package['snapd'],
  }

  # Do not use Debian's deprecated python-certbot-apache
  exec { 'installCertbot':
    command => '/usr/bin/snap install --classic certbot',
    unless  => '/usr/bin/snap list certbot',
    user    => root,
    require => Exec['installCore'],
  }

  file { '/usr/bin/certbot':
    ensure  => link,
    target  => '/snap/bin/certbot',
    owner   => 'root',
    group   => 'root',
    require => Exec['installCertbot'],
  }

  # TODO - Remove that
  file { '/etc/cron.d/certbot':
    ensure => absent,
  }

  $domain = hiera('domainnames')
  letsencrypt::le_certificate { $domain : }

}
