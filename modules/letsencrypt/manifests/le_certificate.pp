# Install Let's Encrypt certificates for title and www.title sites
define letsencrypt::le_certificate
{

  $le_email = hiera('le_email')

  exec { "LECertificate-${title}":
    command => "/usr/bin/certbot --non-interactive --email ${le_email} --agree-tos --domains ${title} --apache certonly",
    # Those files are referenced in the Apache HTTPd configuration
    # Note that this command generates /etc/letsencrypt/options-ssl-apache.conf too (if does not exist)
    creates => [
      "/etc/letsencrypt/live/${title}/fullchain.pem",
      "/etc/letsencrypt/live/${title}/privkey.pem",
    ],
    # We need a functional web server to generate the certificate
    require => [
      Service['apache2'],
      File['/usr/bin/certbot'],
    ],
  }

  exec { "LECertificate-www.${title}":
    command => "/usr/bin/certbot --non-interactive --email ${le_email} --agree-tos --domains www.${title} --apache certonly",
    # Those files are referenced in the Apache HTTPd configuration
    # Note that this command generates /etc/letsencrypt/options-ssl-apache.conf too (if does not exist)
    creates => [
      "/etc/letsencrypt/live/www.${title}/fullchain.pem",
      "/etc/letsencrypt/live/www.${title}/privkey.pem",
    ],
    # We need a functional web server to generate the certificate
    require => [
      Service['apache2'],
      File['/usr/bin/certbot'],
    ],
  }

}
