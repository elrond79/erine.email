#!/usr/bin/python3
"""erine_email

Copyright (C) 2017 Mikael Davranche

This file is part of erine.email project. https://erine.email

erine.email is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with erine.mail.  If not, see <http://www.gnu.org/licenses/>.
"""

import configparser
import erine_email
import logging
import os
import sqlalchemy
import subprocess
import sys
import tempfile
import traceback

from email.parser import BytesParser
from email.policy import compat32
from erine_email.classic import Classic
from erine_email.common import BounceException
from erine_email.firstshot import FirstShot
from erine_email.reply import Reply
from erine_email.reserved import Reserved

DEBUG = False
DEBUG_MAXFILES = 10


def insert_message(db_connection, disposable_mail_address, email, status):
    """Insert a row in the "message" table"""
    query = sqlalchemy.text(
        "INSERT INTO `message` (`messageId`, `disposableMailAddress`, `subject`, `from`, `rcptTo`, `status`) "
        "VALUES (:message_id, :disposable_mail_address, :subject, :_from, :rcpt_to, :status)"
    )
    db_connection.execute(
        query,
        message_id=email.message_id,
        disposable_mail_address=disposable_mail_address,
        subject=email.subject if email.subject else "",
        _from=email.inbound_sender,
        rcpt_to=email.outbound_recipient,
        status=status,
    )


def send(email):
    """Send email using Sendmail"""
    sendmail_cmd = [
        "/usr/sbin/sendmail",
        "-G",
        "-i",
        "-f",
        email.outbound_sender,
        "--",
        "<{0}>".format(email.outbound_recipient),
    ]
    logging.info(sendmail_cmd)
    p = subprocess.Popen(sendmail_cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate(input=email.outbound_message.as_bytes())
    if p.returncode != 0:
        raise Exception("Sendmail returned a {0} code".format(p.returncode))


def main():

    # This script had been designed to be deployed by Puppet. The /var/log spameater/ creation and permissions parts had
    # been transfered on Puppet manifests.
    logging.basicConfig(
        format="%(asctime)s %(levelname)8s [%(process)d] %(message)s",
        level=logging.DEBUG if DEBUG else logging.INFO,
        filename="/var/log/spameater/spameater.log",
    )

    # spameater is called by Postfix using arguments:
    #
    # filter    unix  -       n       n       -       10      pipe
    #        flags=Rq user=spameater null_sender=
    #        argv=/usr/lib/postfix/spameater ${sender} ${recipient}
    #
    # Those arguments are used to rewrite the e-mail and to call sendmail
    #
    # ${sender} and ${recipient} are just the email addresses. They do NOT include the email label nor the <> characters
    # (the format is NOT "Plops <xxx@yyy.zzz>" nor "<xxx@yyy.zzz>" but "xxx@yyy.zzz").
    logging.info(sys.argv)
    if len(sys.argv) != 3:
        raise Exception(str(len(sys.argv)) + " arguments, 3 expected")
    sender = sys.argv[1].lower()
    recipient = sys.argv[2].lower()

    # Retrieve DBMS configuration
    try:
        config = configparser.RawConfigParser()
        config.read_file(open("/etc/erine-email.conf"))
        host = config.get("dbms", "host", fallback="localhost")
        port = int(config.get("dbms", "port", fallback=3306))
        database = config.get("dbms", "database", fallback="spameater")
    except Exception as e:
        logging.warning("Problem retrieving DBMS configuration: {0}".format(str(e)))
        logging.warning("Falling back to spameater database on localhost:3306.")
        host = "localhost"
        port = 3306
        database = "spameater"
    with open("/home/spameater/.mariadb.pwd", "r") as pwd_file:
        password = pwd_file.readline().strip()

    # Connect to the database and begin a transaction
    db_engine = sqlalchemy.create_engine(
        "mysql+mysqldb://spameater:{0}@{1}:{2}/{3}?charset=utf8mb4".format(password, host, port, database)
    )
    db_connection = db_engine.connect().execution_options(autocommit=False)
    db_transaction = db_connection.begin()

    # Postfix feeds this script using a pipe, what means the e-mail is sent on stdin.
    # This script had been designed to be deployed by Puppet. The /tmp/spameater_debug/ creation and permissions parts
    # had been transfered on Puppet manifests.
    if DEBUG and len(os.listdir("/tmp/spameater_debug")) < DEBUG_MAXFILES:
        with tempfile.NamedTemporaryFile(mode="wb", dir="/tmp/spameater_debug", delete=False) as tf:
            stdin_bytes = sys.stdin.buffer.read()
            tf.write(stdin_bytes)
            logging.debug("Inbound email saved to {0}".format(tf.name))
        whole_email = BytesParser(policy=compat32).parsebytes(stdin_bytes)
    else:
        whole_email = BytesParser(policy=compat32).parse(sys.stdin.buffer)

    # Set email, a Classic, Reserved, Reply or FirstShot object ; whole_email will be altered!
    email = erine_email.email(whole_email, sender, recipient, db_connection)
    if DEBUG and len(os.listdir("/tmp/spameater_debug")) < DEBUG_MAXFILES:
        with tempfile.NamedTemporaryFile(mode="wb", dir="/tmp/spameater_debug", delete=False) as tf:
            tf.write(email.outbound_message.as_bytes())
            logging.debug("Outbound email saved to {0}".format(tf.name))

    # Classic or Reserved
    if type(email) in [Classic, Reserved]:
        disposable_mail_address = email.inbound_recipient
        msg_prefix = "{0} - {1}".format("Classic" if type(email) == Classic else "Reserved", disposable_mail_address)

    # Reply or FirstShot
    else:
        disposable_mail_address = email.outbound_sender
        msg_prefix = "{0} - {1}".format("Reply" if type(email) == Reply else "FirstShot", disposable_mail_address)

    # Exit if message already processed for this disposable mail address
    query = sqlalchemy.text(
        "SELECT 1 FROM `message` WHERE `messageId` = :message_id AND `disposableMailAddress` = :disposable_mail_address"
    )
    results = db_connection.execute(query, message_id=email.message_id, disposable_mail_address=disposable_mail_address)
    if results.one_or_none():
        raise BounceException(
            "{0} - {1} already processed for {2}".format(msg_prefix, email.message_id, disposable_mail_address)
        )

    # Set enabled
    # The record necessarily exists: erine_email.email() already checked that
    query = sqlalchemy.text("SELECT `enabled` FROM `disposableMailAddress` WHERE `mailAddress` = :mail_address")
    results = db_connection.execute(query, mail_address=disposable_mail_address)
    row = results.one_or_none()
    enabled = row._mapping["enabled"]

    # Send or drop the email
    if type(email) in [Classic, Reserved]:
        if enabled:
            logging.info("{0} (enabled) - Sending {1}...".format(msg_prefix, email.message_id))
            query = sqlalchemy.text(
                "UPDATE `disposableMailAddress` SET `sent` = `sent` + 1 WHERE `mailAddress` = :mail_address"
            )
            db_connection.execute(query, mail_address=disposable_mail_address)
            insert_message(db_connection, disposable_mail_address, email, "sent")
            send(email)
            logging.info("{0} (enabled) - {1} sent".format(msg_prefix, email.message_id))
        else:
            logging.info("{0} (disabled) - Dropping {1}...".format(msg_prefix, email.message_id))
            query = sqlalchemy.text(
                "UPDATE `disposableMailAddress` SET `dropped` = `dropped` + 1 WHERE `mailAddress` = :mail_address"
            )
            db_connection.execute(query, mail_address=disposable_mail_address)
            insert_message(db_connection, disposable_mail_address, email, "dropped")
            logging.info("{0} (disabled) - {1} dropped".format(msg_prefix, email.message_id))
    elif type(email) in [Reply, FirstShot]:
        if enabled:
            logging.info("{0} (enabled) - Sending {1}...".format(msg_prefix, email.message_id))
        else:
            logging.warning("{0} (disabled! sending anyway) - Sending {1}...".format(msg_prefix, email.message_id))
        query = sqlalchemy.text(
            "UPDATE `disposableMailAddress` SET `sentAs` = `sentAs` + 1 WHERE `mailAddress` = :mail_address"
        )
        db_connection.execute(query, mail_address=disposable_mail_address)
        insert_message(db_connection, disposable_mail_address, email, "sentAs")
        send(email)
        if enabled:
            logging.info("{0} (enabled) - {1} sent".format(msg_prefix, email.message_id))
        else:
            logging.warning("{0} (disabled! sending anyway) - {1} sent".format(msg_prefix, email.message_id))

    # Commit the transaction and close the connection to the database
    db_transaction.commit()
    db_connection.close()


if __name__ == "__main__":
    global db_connection
    try:
        main()
    except BounceException as e:
        logging.warning("Bouncing email: {0}".format(str(e)))

        # The email is bounced (dropped by Postfix) by terminating with exit status 69
        # More information on https://www.postfix.org/pipe.8.html and sysexits.h
        # Also, implicitely close the connection to the DBMS and rollback undelying transaction, if any
        sys.exit(69)

    except Exception as e:
        logging.critical("Deferring email: {0} ; Traceback:".format(str(e)))
        tb_list = traceback.extract_tb(e.__traceback__)
        for tb in tb_list:
            logging.critical("- {0}, line {1}: {2}".format(tb.filename, tb.lineno, tb.line))

        # The email is deferred (Postfix will try to send it again later) by terminating with exit status 75
        # More information on https://www.postfix.org/pipe.8.html and sysexits.h
        # Also, implicitely close the connection to the DBMS and rollback undelying transaction, if any
        sys.exit(75)
