<?php
include('../core/config.php');

if (count($_POST))
{
  $input = new \ptejada\uFlex\Collection($_POST);
  $hash = $input->c;
  if (!$user->isSigned() and $hash)
  {

    // Change password with confirmation hash
    $user->newPassword(
      $hash,
      array(
        'password'  => $input->password,
        'password2' => $input->password2,
      )
    );
  }
  else
  {

    // Change the password of signed in user without a confirmation hash
    $user->update(
      array(
        'password'  => $input->password,
        'password2' => $input->password2,
      )
    );
  }
  echo json_encode(
    array(
      'error'   => $user->log->getAllErrors(),
      'confirm' => 'Password changed successfully',
      'form'    => $user->log->getFormErrors(),
    )
  );
}
?>
