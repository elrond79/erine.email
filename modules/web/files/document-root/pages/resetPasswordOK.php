<?php
  if($user->isSigned()) redirect("/disposableMails");
?>
<div class="content-parent">
  <div class="content-child-middle">
    <div class="container marketing">
      <i class="fa-solid fa-paper-plane fa-ee-message"></i>
      <h2>Confirmation sent</h2>
      <p>An URL had been sent by email. Please check your inbox and clic on the provided link to choose your new password.</p>
      <p><a class="btn btn-viewdetails" href="/" role="button">Home &raquo;</a></p>
    </div>
  </div>
</div>
