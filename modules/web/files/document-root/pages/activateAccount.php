<?php
$hash = "";
if (count($_GET))
{
  $input = new \ptejada\uFlex\Collection($_GET);
  $hash = $input->c;
}
if ($hash and $user->activate($hash))
{
?>
<div class="content-parent">
  <div class="content-child-middle">
    <div class="container marketing">
      <i class="fa-solid fa-paper-plane fa-ee-message"></i>
      <h2>Account activated!</h2>
      <p>You can now login to start the adventure!</p>
      <p><a class="btn btn-viewdetails" href="/login" role="button">Got it! &raquo;</a></p>
    </div>
  </div>
</div>
<?php
}
else
{
  redirect("/error");
}
?>
