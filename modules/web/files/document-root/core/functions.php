<?php

/**
 * Redirects the user
 *
 * @param bool|string $url
 * @param int         $time
 */
function redirect($url = false, $time = 0)
{
    $url = $url ? $url : $_SERVER['HTTP_REFERER'];

    if (!headers_sent()) {
        if (!$time) {
            header("Location: {$url}");
        } else {
            header("refresh: $time; {$url}");
        }
    } else {
        echo "<script> setTimeout(function(){ window.location = '{$url}' }," . ($time * 1000) . ")</script>";
    }
}

/* Prepare and execute a SQL query. Redirect to the error page on failure. */
function sqlquery($pdo = FALSE, $query = "", $args = [])
{
  try
  {
    $stmt = $pdo->prepare($query);
    $stmt->execute($args);
    return $stmt;
  }
  catch(PDOException $ex)
  {
    error_log($ex);
    redirect("/error");
  }
}
